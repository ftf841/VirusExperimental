package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/**
 * Created by mzhang on 6/21/2018.
 */

public abstract class VirusOpMode extends LinearOpMode {
    public String opModeType="loop";
    public abstract void body();
    public abstract void initialize();
    public void preLoop(){}
    public void runOpMode() throws InterruptedException{
        initialize();
        waitForStart();
        if(opModeType=="loop"){
            preLoop();
            while(opModeIsActive()){
                body();
                idle();
            }
        }
        else if(opModeType=="linear"){
            body();
        }
        else{
            telemetry.addData("OpMode incorrectly defined", "whoops");
            Thread.sleep(2000);
        }
    }
}

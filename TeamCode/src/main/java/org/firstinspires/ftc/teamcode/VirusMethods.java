package org.firstinspires.ftc.teamcode;

import android.view.OrientationEventListener;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.AccelerationSensor;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;

public abstract class VirusMethods extends VirusHardware{

    int counter=0;
    double turnRate;
    double angleRel;
    double threshold = .25;
    @Override
    public void initialize(){
        initializeIMU();
        super.initialize();
    }
    //move motors at a certain speed, and specify a steering amount
    public void runMotors(double Left0, double Left1, double Right0, double Right1, double steerMagnitude){
        if (Left0!=0&&Left1!=0&&Right0!=0&&Right1!=0) {
            steerMagnitude *= 2 * Math.max(Math.max(Left0, Left1), Math.max(Right0, Right1));
        }
        Left0=Left0+maxSteerPower*steerMagnitude;
        Left1=Left1+maxSteerPower*steerMagnitude;
        Right0=Right0-maxSteerPower*steerMagnitude;
        Right1=Right1-maxSteerPower*steerMagnitude;
        //make sure no exception thrown if power > 0
        Left0 = Range.clip(Left0, -maxPower, maxPower);
        Left1 = Range.clip(Left1, -maxPower, maxPower);
        Right0 = Range.clip(Right0, -maxPower, maxPower);
        Right1 = Range.clip(Right1, -maxPower, maxPower);
        rmotor0.setPower(Right0);
        rmotor1.setPower(Right1);
        lmotor0.setPower(Left0);
        lmotor1.setPower(Left1);
    }
    //move motors at certain speed
    public void runMotors(double Left0, double Left1, double Right0, double Right1){
        lmotor0.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lmotor1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rmotor0.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rmotor1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //make sure no exception thrown if power > 0
        Left0 = Range.clip(Left0, -maxPower, maxPower);
        Left1 = Range.clip(Left1, -maxPower, maxPower);
        Right0 = Range.clip(Right0, -maxPower, maxPower);
        Right1 = Range.clip(Right1, -maxPower, maxPower);
        rmotor0.setPower(Right0);
        rmotor1.setPower(Right1);
        lmotor0.setPower(Left0);
        lmotor1.setPower(Left1);
    }
    public void runMotorsAuto(double Left0, double Left1, double Right0, double Right1){
        //make sure no exception thrown if power > 0
        Left0 = Range.clip(Left0, -maxPower, maxPower);
        Left1 = Range.clip(Left1, -maxPower, maxPower);
        Right0 = Range.clip(Right0, -maxPower, maxPower);
        Right1 = Range.clip(Right1, -maxPower, maxPower);
        rmotor0.setPower(Right0);
        rmotor1.setPower(Right1);
        lmotor0.setPower(Left0);
        lmotor1.setPower(Left1);
    }
    //make the motors go to a certain encoder position
    public boolean setMotorPositions(int Left0, int Left1, int Right0, int Right1, double power) {
        if (counter == 0) { //makes sure this is only run once, reset back to 0 when OpMode starts or resetEncoders is called
            lmotor0.setTargetPosition(Left0);
            lmotor1.setTargetPosition(Left1);
            rmotor0.setTargetPosition(Right0);
            rmotor1.setTargetPosition(Right1);
            runMotorsAuto(power, power, power, power);
            counter++;
        }
        return !lmotor0.isBusy() && !lmotor1.isBusy() && !rmotor0.isBusy() && !rmotor1.isBusy(); //returns true when motors are not busy
    }
    //make the robot move a certain distance in inches
    public boolean setMotorPositionsINCH(double Left0, double Left1, double Right0, double Right1, double power){
        lmotor0.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        lmotor1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rmotor0.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rmotor1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        if (counter == 0){ //makes sure this is only run once, reset back to 0 when OpMode starts or resetEncoders is called
            lmotor0.setTargetPosition((int)(Left0/inPerPulse));
            lmotor1.setTargetPosition((int)(Left1/inPerPulse));
            rmotor0.setTargetPosition((int)(Right0/inPerPulse));
            rmotor1.setTargetPosition((int)(Right1/inPerPulse));
            runMotorsAuto(power,power,power,power);
            counter++;
        }
        return (!lmotor0.isBusy() && !lmotor1.isBusy() && !rmotor0.isBusy() && !rmotor1.isBusy()); //returns true when motors are not busy

    }
    //make the robot face a certain direction, accepts an angle between [0, 360) and a speed from 0-1
    public boolean turn(double angle, double speed){
        angle=360-angle;
        double currentAngle = getZHeading();
        angleRel = relativeAngle(angle, currentAngle); //should be distance from current angle (negative if to the counterclockwise, positive if to the clockwise)
        turnRate = speed*angleRel/90;
        if (turnRate<0){
            turnRate-=.025;
        }
        else if(turnRate>0){
            turnRate+=.025;
        }
        runMotors(turnRate, turnRate, -turnRate, -turnRate); //negative turnRate will result in a left turn
        if (angleRel<=threshold && angleRel>=-threshold) { //approaching from either side
            return true;
        }
        return false;
    }
    //sets the turning threshold
    public void setThreshold(double newThreshold){
        threshold = newThreshold;
    }
    //some math thing that carlos did idk
    private double relativeAngle(double angle, double currentAngle){
        double currentAngleRel = angle-currentAngle;
        if (currentAngleRel > 180){
            currentAngleRel -= 360;
        }else if (currentAngle < -179){
            currentAngleRel += 360;
        }
        return currentAngleRel;
    }
    //sets all encoder values to 0
    public void resetEncoder(){
        runMotors(0,0,0,0);
        lmotor0.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lmotor1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rmotor0.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rmotor1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        counter=0; // sets counter = 0 for setMotorPosition method
        while (lmotor0.isBusy()||lmotor1.isBusy()||rmotor0.isBusy()||rmotor1.isBusy()); //waits until encoders finish reset
        lmotor0.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        lmotor1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rmotor0.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rmotor1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }
    //includes math for mecanum teleop control
    public void updateControllerValues(){
        lefty = -gamepad1.left_stick_y;
        leftx = -gamepad1.left_stick_x;
        righty = -gamepad1.right_stick_y;
        rightx = -gamepad1.right_stick_x;
        rtrigger = -gamepad1.right_trigger;
        ltrigger = -gamepad1.left_trigger;
        /* here i am
            writing these comments
            will anyone give a damn
            does anyone read these laments
         */
        double scalar = Math.max(Math.abs(lefty-leftx), Math.abs(lefty+leftx)); //scalar and magnitude scale the motor powers based on distance from joystick origin
        double magnitude = Math.sqrt(lefty*lefty+leftx*leftx);
        var1= (lefty-leftx)*magnitude/scalar;
        var2= (lefty+leftx)*magnitude/scalar;
        var1*=maxPower;
        var2*=maxPower;
    }
    //initialize the IMU
    public void initializeIMU(){
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "AdafruitIMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imu.initialize(parameters);
    }

    //gets the Z heading with values corrected to work with the turn method
    public double getZHeading(){
        if(Orientation.firstAngle<0){
            return 360+Orientation.firstAngle;
        }else if(Orientation.firstAngle>0){
            return Orientation.firstAngle;
        }
        else{
            return 0;
        }
    }
    //returns raw IMU Z Heading
    public double getRawZHeading(){
        return Orientation.firstAngle;
    }

    //returns raw IMU pitch value
    public double getPitch(){
        //Orientation = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return Orientation.secondAngle-initialPitch;
    }

    //returns raw IMU roll value
    public double getRoll(){
        //Orientation = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return Orientation.thirdAngle-initialRoll;
    }

    //returns red value
    public double getRed(){return colorSensor.red();}

    //returns blue value adjusted for sensor bias
    public double getBlue(){return colorSensor.blue()+20;}

    //prints out some cool™ stuff to telemetry
    public void Telemetry(){
        telemetry.addData("Red",colorSensor.red());
        telemetry.addData("Green",colorSensor.green());
        telemetry.addData("Blue",colorSensor.blue());

        telemetry.addData("lMotor0 Encoder",lmotor0.getCurrentPosition());
        telemetry.addData("lMotor1 Encoder",lmotor1.getCurrentPosition());
        telemetry.addData("rMotor0 Encoder",rmotor0.getCurrentPosition());
        telemetry.addData("rMotor1 Encoder",rmotor1.getCurrentPosition());

        telemetry.addData("lMotor0 Target",lmotor0.getTargetPosition());
        telemetry.addData("lMotor1 Target",lmotor1.getTargetPosition());
        telemetry.addData("rMotor0 Target",rmotor0.getTargetPosition());
        telemetry.addData("rMotor1 Target",rmotor1.getTargetPosition());
    }

    //grabs the current orientation from the IMU and updates the robot's orientation
    public void updateOrientation (){
        Orientation = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZXY, AngleUnit.DEGREES);
    }
    public void balance(){
        final double constant = -0.014;
        double left0 = getRoll()*constant, left1 = getRoll()*constant, right0 = getRoll()*constant, right1 = getRoll()*constant;
        runMotors(left0,left1,right0,right1);
    }

}
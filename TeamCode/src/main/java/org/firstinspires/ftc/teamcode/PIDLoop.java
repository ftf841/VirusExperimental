package org.firstinspires.ftc.teamcode;

/**
 * Created by mzhang on 6/19/2018.
 */

public class PIDLoop{
    private double kp;
    private double ki;
    private double kd;
    private double error=0;
    private double error_prior=0;
    private double integral=0;
    private double derivative=0;
    private double output=0;
    private double target;

    PIDLoop(double kpIn, double kdIn, double kiIn){
        kp=kpIn;
        kd=kdIn;
        ki=kiIn;
    }
    public double correction(double target, double input, double aquisitionTime,  double bias){
        error = target-input;
        integral = integral+(error*aquisitionTime);
        derivative = (error-error_prior)/aquisitionTime;
        output = kp*error + ki*integral + kd*derivative + bias;
        error_prior = error;
        //sleep(iteration_time);
        return output;
    }
    public void setTarget(double newTarget){
        target=newTarget;
    }
}

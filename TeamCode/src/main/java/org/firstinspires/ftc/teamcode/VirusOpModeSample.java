package org.firstinspires.ftc.teamcode;

/**
 * Example of VirusOpMode structure
 */

public class VirusOpModeSample extends VirusMethods {
    public void intialize(){
        super.initialize();
        opModeType="linear";
        //initialization code here
    }

    @Override
    public void body() {
        //main code here
    }


}
